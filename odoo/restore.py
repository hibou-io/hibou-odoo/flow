import os
import base64
import secrets
import urllib.request
import shutil
import uuid

from flow.odoo.common import parser
from flow.tools.run import run
from flow.tools.minio import s3_uri, minio_client, MinioSseCustomerKey
from flow.tools.postgres import db_connect, db_create, db_drop, exec_pg_environ, ISOLATION_LEVEL_AUTOCOMMIT, PGOperationalError


parser.add_argument('--backup_file', dest='backup_file', type=str, default='',
                    help='location of backup file (or where it will be downloaded to)')
parser.add_argument('--backup_format', dest='backup_format', type=str, default='',
                    help='format of backup')
parser.add_argument('--existing_file', dest='existing_file', type=str, default='',
                    help='behavior if the backup_file already exists (delete, skip, halt)')
parser.add_argument('--finished_file', dest='finished_file', type=str, default='delete',
                    help='behavior of backup_file after operation complete (keep, delete)')
parser.add_argument('--restore_type', dest='restore_type', type=str, default='copy',
                    help='behavior of restore, copy means the db was copied and will get new DBUUID, move will not')
parser.add_argument('--restore_test', dest='restore_test', type=str, default='',
                    help='behavior of restore for test, if set will archive scheduled actions etc.')
parser.add_argument('--restore_sql', dest='restore_sql', type=str, default='',
                    help='after database restore, sql file to run against it')
parser.add_argument('--skip_db', dest='skip_db', type=str, default='',
                    help='skip restoring the database')
parser.add_argument('--skip_fs', dest='skip_fs', type=str, default='',
                    help='skip restoring the filestore')
parser.add_argument('--existing_db', dest='existing_db', type=str, default='',
                    help='behavior if database exists. (delete, skip, halt) (Note that the filestore will '
                         'still be merged if it is in the database unless you also use --skip_fs 1')
parser.add_argument('--core_count', dest='core_count', type=str, default='4',
                    help='Core count to use when restoring custom dumps. (only for custom format directory dump)')

# Source Arguments
parser.add_argument('--backup_src', dest='backup_src', type=str, default='',
                    help='destination of backup (s3:// url, upgrade)')
parser.add_argument('--backup_minio_host', dest='backup_minio_host', type=str, default='',
                    help='when using s3:// dest, S3 host')
parser.add_argument('--backup_minio_port', dest='backup_minio_port', type=str, default='',
                    help='when using s3:// dest, S3 host port')
parser.add_argument('--backup_minio_region', dest='backup_minio_region', type=str, default='us-west-1',
                    help='when using s3:// dest, S3 region')
parser.add_argument('--backup_minio_access_key', dest='backup_minio_access_key', type=str, default='',
                    help='when using s3:// dest, S3 access key')
parser.add_argument('--backup_minio_secret_key', dest='backup_minio_secret_key', type=str, default='',
                    help='when using s3:// dest, S3 secret key')
parser.add_argument('--backup_minio_secure', dest='backup_minio_secure', type=str, default='',
                    help='when using s3:// dest, S3 over https')
parser.add_argument('--backup_minio_sse', dest='backup_minio_sse', type=str, default=None,
                    help='when using s3:// dest, consumer encryption key (32 char of base64)')
args = parser.parse_args()

# the Odoo standard arguments
db_name, db_user, db_host, db_port, db_password, data_dir = args.db_name, args.db_user, args.db_host, args.db_port, args.db_password, args.data_dir
other_db_name = args.other_db_name
if other_db_name:
    # we have to trust the cli flag users have control over (i.e. not from config file)
    db_name = other_db_name

# my new ones
backup_file, backup_format, existing_file, finished_file, restore_type, restore_test, restore_sql, skip_db, skip_fs, existing_db, core_count = args.backup_file, args.backup_format, args.existing_file, args.finished_file, args.restore_type, args.restore_test, args.restore_sql, args.skip_db, args.skip_fs, args.existing_db, args.core_count
# source
backup_src = args.backup_src
backup_minio_host, backup_minio_port, backup_minio_region, backup_minio_access_key, backup_minio_secret_key, backup_minio_secure, backup_minio_sse = args.backup_minio_host, args.backup_minio_port, args.backup_minio_region, args.backup_minio_access_key, args.backup_minio_secret_key, args.backup_minio_secure, args.backup_minio_sse

if restore_test and restore_type != 'copy':
    raise Exception('--restore_test should only be used with --restore_type copy')

if not backup_format and not backup_file:
    # the true defaults
    backup_file = 'backup'
    backup_format = 'zip'
elif not backup_format:
    if backup_file.endswith('.sql.gz'):
        backup_format = 'sql.gz'
    elif backup_file.endswith('.sql'):
        backup_format = 'sql'
    elif backup_file.endswith('.zip'):
        backup_format = 'zip'
    else:
        raise Exception('backup_format not detected in backup_file %s' % (backup_file, ))

if not backup_file:
    # Note we don't need zip on the end as shutil will do that
    backup_file = 'backup'

if backup_file.find('/') < 0:
    backup_file = '/tmp/' + backup_file

# in case someone added extension by hand
backup_file = backup_file.replace('.' + backup_format, '')
# build expected file name
backup_file_real = backup_file + '.' + backup_format

if not backup_src and not existing_file:
    existing_file = 'skip'
if backup_src and not existing_file:
    existing_file = 'delete'

# If we have the file and want to delete it first
if os.path.exists(backup_file_real) and existing_file == 'delete':
    os.remove(backup_file_real)

# If we have the file and cannot skip
if os.path.exists(backup_file_real) and existing_file != 'skip':
    raise Exception('backup_file exists (%s) and we are not allowed to skip it' % (backup_file_real, ))

if not os.path.exists(backup_file_real) and backup_src:
    # Is S3?
    s3_bucket, s3_item = None, None
    try:
        s3_bucket, s3_item = s3_uri(backup_src)
    except ValueError:
        pass
    if s3_item:
        if backup_minio_port:
            backup_minio_host = backup_minio_host + ':' + backup_minio_port
        client = minio_client(backup_minio_host, backup_minio_region, backup_minio_access_key,
                              backup_minio_secret_key, backup_minio_secure)
        if backup_minio_sse is not None:
            if backup_minio_sse == 'generate':
                backup_minio_sse = base64.b64encode(secrets.token_bytes(32))
                backup_minio_sse = backup_minio_sse[:32]
                print('Generated SSE-C: ' + str(backup_minio_sse.decode()))
            backup_minio_sse = MinioSseCustomerKey(backup_minio_sse)
        print('Starting Minio Download.')
        client.fget_object(s3_bucket, s3_item, backup_file_real, sse=backup_minio_sse)
        print('Finished Minio Download.')
    elif backup_src.startswith('http'):
        print('Starting download over HTTP(S)')
        urllib.request.urlretrieve(backup_src, backup_file_real)
        print('Downloaded over HTTP(S)')
    else:
        raise Exception('Unknown how to retrieve %s' % (backup_src, ))

if not os.path.exists(backup_file_real):
    raise Exception('Backup File does not exist! %s' % (backup_file_real, ))

print('Starting Restore from %s' % (backup_file_real, ))

found_database = False
found_database_table_count = 0
if not skip_db:
    print('Attempting connection to inspect existing database...')
    try:
        with db_connect(db_name, db_user, db_password, db_host, db_port) as db:
            db.set_isolation_level(ISOLATION_LEVEL_AUTOCOMMIT)
            with db.cursor() as cr:
                cr.execute('SELECT count(*) FROM pg_class c JOIN pg_namespace s ON s.oid = c.relnamespace '
                           'WHERE s.nspname not in (\'pg_catalog\', \'information_schema\') '
                           'AND s.nspname not like \'pg_temp%\' '
                           'AND s.nspname not like \'pg_toast%\' '
                           'AND c.relname not in (\'pg_stat_statements\') ')
                table_count_res = cr.fetchall()
                found_database = True
                if table_count_res and table_count_res[0]:
                    found_database_table_count = table_count_res[0][0]
    except PGOperationalError:
        print('  connection PGOperationalError, assuming database does not exist.')

if found_database and found_database_table_count:
    if existing_db == 'skip':
        skip_db = True
    elif existing_db in ('delete', 'drop'):
        print('Existing database found, dropping...')
        db_drop(db_name, db_user, db_password, db_host, db_port)
    else:
        # halt is an option, but it would be the same as any unknown behavior
        raise Exception('Existing database (with table count %s) found with strategy "%s".  Halting!' % (found_database_table_count, existing_db))

if backup_format in ('zip', 'dump.zip'):
    print('Unzipping...')
    shutil.unpack_archive(backup_file_real, backup_file, 'zip')
    if not skip_db:
        args = None
        dump_path = os.path.join(backup_file, 'dump.sql')
        if os.path.exists(dump_path):
            print('Restoring dump.sql')
            args = ['psql', '--file=' + dump_path, '-d', db_name]
        else:
            dump_path = os.path.join(backup_file, 'dump')
            if os.path.exists(dump_path) and os.path.isdir(dump_path):
                print('Restoring custom dump directory')
                args = ['pg_restore', '--no-owner', '-Fd', dump_path, '-d', db_name, '-j', core_count]
        if args:
            p = run(args, env=exec_pg_environ(db_user, db_password, db_host, db_port), check=False)
            # What happens if the database does not exist?
            # pg_restore returns 1
            # psql returns 2
            if p.returncode in (1, 2):
                print('Attempting create database "%s"' % (db_name, ))
                db_create(db_name, db_user, db_password, db_host, db_port)
                print('  restoring after db_create')
                run(args, env=exec_pg_environ(db_user, db_password, db_host, db_port))
            else:
                p.check_returncode()

    filestore_path = os.path.join(backup_file, 'filestore')
    if os.path.exists(filestore_path) and not skip_fs:
        print('Restoring filestore')
        filestore_dest = os.path.join(data_dir, 'filestore', db_name)
        if not os.path.exists(filestore_dest):
            run(['mkdir', '-p', filestore_dest])
        directories = os.listdir(filestore_path)
        for dir_ in directories:
            dir_ = os.path.join(filestore_path, dir_)
            run(['cp', '-r', dir_, filestore_dest])

elif backup_format in ('sql.gz', 'sql') and not skip_db:
    if backup_format == 'sql.gz':
        run(['gunzip', backup_file_real])
        backup_file_real.replace('.gz', '')
    print('Restoring %s' % (backup_file_real, ))
    args = ['psql', '--file=' + backup_file_real, '-d', db_name]
    p = run(args, env=exec_pg_environ(db_user, db_password, db_host, db_port), check=False)
    if p.returncode == 2:
        print('Attempting create database "%s"' % (db_name,))
        db_create(db_name, db_user, db_password, db_host, db_port)
        run(args, env=exec_pg_environ(db_user, db_password, db_host, db_port))
    else:
        p.check_returncode()
else:
    raise Exception('backup_format %s unknown' % (backup_format, ))

if restore_type == 'copy' and not skip_db:
    print('restore_type == copy...')
    with db_connect(db_name, db_user, db_password, db_host, db_port) as db:
        db.set_isolation_level(ISOLATION_LEVEL_AUTOCOMMIT)
        with db.cursor() as cr:
            uuid = str(uuid.uuid1())
            print('  setting database.uuid=%s' % (uuid, ))
            cr.execute('UPDATE ir_config_parameter SET value = %s WHERE key = %s', (uuid, 'database.uuid'))
    if restore_test:
        if restore_test == '1':
            restore_test = '/flow/odoo/restore_test.sql'
        if os.path.isfile(restore_test):
            args = ['psql', '--file=' + restore_test, '-d', db_name]
            run(args, env=exec_pg_environ(db_user, db_password, db_host, db_port))
        else:
            print('  cannot find restore_test file %s' % (restore_test, ))

if restore_sql and os.path.isfile(restore_sql):
    print('running restore_sql additional sql file "%s"' % (restore_sql, ))
    args = ['psql', '--file=' + restore_sql, '-d', db_name]
    run(args, env=exec_pg_environ(db_user, db_password, db_host, db_port))

# Clean up
print('Cleanup...')
if os.path.exists(backup_file_real) and finished_file == 'delete':
    os.remove(backup_file_real)
if os.path.exists(backup_file) and finished_file == 'delete':
    if os.path.isdir(backup_file):
        shutil.rmtree(backup_file, ignore_errors=True)
    else:
        os.remove(backup_file)
print('Done!')
