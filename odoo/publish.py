import argparse
import re
import os
import json
import markdown
from urllib.request import urlopen, Request
import pprint


CI_ODOO_URL = os.getenv('CI_ODOO_URL', '')
CI_ODOO_REPO_TOKEN = os.getenv('CI_ODOO_REPO_TOKEN', '')
CI_PROJECT_URL = os.getenv('CI_PROJECT_URL', '')

parser = argparse.ArgumentParser()

parser.add_argument('-a', '--author', dest='repo_author', type=str, default='Hibou Corp.',
                    help='Author')
parser.add_argument('-l', '--license', dest='repo_license', type=str, default='AGPL-3',
                    help='License')
parser.add_argument('-n', '--name', dest='repo_name', type=str,
                    help='Name')
parser.add_argument('-v', '--version', dest='repo_version', type=str,
                    help='Version')

# odoo_url
# is ($CI_ODOO_URL)
parser.add_argument('--odoo-url', dest='odoo_url', type=str, default=CI_ODOO_URL,
                    help='Odoo URL to publish to.')

# odoo_token
# is ($CI_ODOO_REPO_TOKEN)
parser.add_argument('--odoo-token', dest='odoo_token', type=str, default=CI_ODOO_REPO_TOKEN,
                    help='Odoo Publish Auth Token')

# repo_url
# is ($CI_PROJECT_URL)
parser.add_argument('--repo-url', dest='repo_url', type=str, default=CI_PROJECT_URL,
                    help='Repository URL')

parser.add_argument('--repo-path', dest='repo_path', type=str, default='/src',
                    help='Repository Path on Disk')

args = parser.parse_args()


odoo_url, odoo_token, repo_url, repo_path = args.odoo_url, args.odoo_token, args.repo_url, args.repo_path
repo_author, repo_license, repo_name, repo_version = args.repo_author, args.repo_license, args.repo_name, args.repo_version

if not all([
    odoo_url, odoo_token, repo_url, repo_path,
    repo_author, repo_license, repo_name, repo_version
]):
    raise Exception('Missing parameter!\nUse -h to learn more.\n' + str(args))

repo_data = {
    'url': repo_url,
    'author': repo_author,
    'version': repo_version,
    'name': repo_name,
    'license': repo_license,
    'description': '',
    'modules': [],
}

directories = [x for x in os.walk(repo_path)]
main_directory = directories[0][0]
if not main_directory.endswith('/'):
    main_directory += '/'
first_directories = directories[0][1]
for module_name in first_directories:
    walk_pieces = [x for x in os.walk(main_directory + module_name)]
    files = []
    if len(walk_pieces) >= 1 and len(walk_pieces[0]) >= 3:
        files = walk_pieces[0][2]
    if '__init__.py' in files and ('__manifest__.py' in files or '__openerp__.py' in files):
        if '__manifest__.py' in files:
            file_name = '__manifest__.py'
        else:
            file_name = '__openerp__.py'
        with open(main_directory + module_name + '/' + file_name, 'r') as manifest:
            manifest_data = eval(manifest.read())
            author = manifest_data.get('author', '')
            author = re.sub(r'(\s*<[^<>]*>\s*)', '', author)
            description = manifest_data.get('description', '')
            description = markdown.markdown(description)
            module_data = {
                'name': manifest_data.get('name'),
                'name_technical': module_name,
                'author': author,
                'license': manifest_data.get('license', 'AGPL-3'),
                'description': description,
            }
            repo_data['modules'].append(module_data)


pprint.pprint(repo_data)

payload = {
    'auth_key': odoo_token,
    'repo_data': [repo_data],  # Note the strangeness here from the original use case of having several repos
}

request = Request(odoo_url + '/docs/repo_import')
request.add_header('Content-Type', 'application/json')
response = urlopen(request, json.dumps({'params': payload}).encode())

print('Response: ' + str(response.read()))
