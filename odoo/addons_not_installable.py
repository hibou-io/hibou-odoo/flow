import os
import re

from flow.odoo.common import parser, dump_db_manifest


parser.add_argument('--work-dir', dest='work_dir', default='/opt/odoo/addons', help='directory where modules exist')
parser.add_argument('--recursive', dest='recursive', default=True, const=True, nargs='?', help='should walk up directories')

args = parser.parse_args()

work_dir, recursive = args.work_dir, args.recursive

def replace_installable_true_with_false(base_directory, recursive):
    """
    Find all __manifest__.py files, search for 'installable': True or "installable": True,
    and replace True with False.
    """
    for root, _, files in os.walk(base_directory):
        if "__manifest__.py" in files:
            manifest_path = os.path.join(root, "__manifest__.py")
            print(f"Processing: {manifest_path}")
            try:
                with open(manifest_path, 'r') as file:
                    content = file.read()

                # Replace 'installable': True or "installable": True with False
                new_content = re.sub(r"(['\"]installable['\"]\s*:\s*)True", r"\1False", content)

                if content != new_content:
                    with open(manifest_path, 'w') as file:
                        file.write(new_content)
                    print(f"Updated: {manifest_path}")
                else:
                    print(f"No changes needed: {manifest_path}")
            except Exception as e:
                print(f"Error processing {manifest_path}: {e}")
        if not recursive:
            break

replace_installable_true_with_false(work_dir, recursive)
