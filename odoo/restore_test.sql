-- archive all Scheduled Actions
UPDATE ir_cron SET active=false;

-- delete all mail servers
DELETE FROM ir_mail_server;
DELETE FROM fetchmail_server;

-- Examples!
-- Note - You will need to ensure the tables exist, and that the commands have the correct behavior
-- If it errors, then the already restored database can leak the things you're trying to get rid of.

-- Delete all sale orders
-- DELETE FROM sale_order;

-- Reset admin password (or maybe ALL users with no WHERE)
-- UPDATE res_users SET password = 'test' WHERE login = 'admin';
