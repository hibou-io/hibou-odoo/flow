from flow.tools.args import ArgumentParser

parser = ArgumentParser()

parser.add_argument('--db_name', dest='db_name', type=str,
                    help='postges database name')
parser.add_argument('--db_host', dest='db_host', type=str,
                    help='postges database host')
parser.add_argument('--db_port', dest='db_port',  type=str,
                    help='postges database host port')
parser.add_argument('--db_user', dest='db_user', type=str,
                    help='postges database user name')
parser.add_argument('--db_password', dest='db_password', type=str,
                    help='postges database password')
parser.add_argument('--data_dir', dest='data_dir', type=str,
                    help='odoo data_dir')

parser.add_argument('-d', dest='other_db_name', type=str,
                    help='cli flag for Odoo runtime postgres database name')
parser.add_argument('-i', dest='other_init', type=str,
                    help='cli flag for Odoo runtime init modules')
parser.add_argument('-u', dest='other_upgrade', type=str,
                    help='cli flag for Odoo runtime upgrade modules')


def dump_db_manifest(cr, db_name='odoo'):
    pg_version = "%d.%d" % divmod(cr.connection.server_version / 100, 100)
    cr.execute("SELECT name, latest_version FROM ir_module_module WHERE state = 'installed'")
    modules = dict(cr.fetchall())
    try:
        import odoo.release
        version = odoo.release.version
        version_info = odoo.release.version_info
        major_version = odoo.release.major_version
    except ImportError as e:
        print('Cannot import odoo.release (using defaults)' + str(e))
        version = '0.0'
        version_info = '0.0'
        major_version = '0.0'

    manifest = {
        'odoo_dump': '1',
        'db_name': db_name,
        'version': version,
        'version_info': version_info,
        'major_version': major_version,
        'pg_version': pg_version,
        'modules': modules,
    }
    return manifest
