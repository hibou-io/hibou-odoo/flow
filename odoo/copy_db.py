import os
import uuid
from subprocess import Popen, PIPE

from flow.tools.run import run
from flow.tools.postgres import db_connect, db_create, db_drop, exec_pg_environ, ISOLATION_LEVEL_AUTOCOMMIT, PGOperationalError, PGObjectInUse

from flow.odoo.common import parser, dump_db_manifest


# Very important that this database config represents the DESTINATION of the copy
# This is motivated by a use case like in an 'odoo' build that includes flow, where you may want to copy then upgrade EVERYTHING
# for example as an automatically created staging branch from production (or possibly an Openupgrade or Odoo EE Upgrade)
# /entrypoint.sh flow odoo-copy --source_db_name odoo-11-0 --copy_test 1 -- -u base --stop-after-init
# of both of these things exit 0, then a "Job" could be said to have completed successfully,
# and producing whatever logged output they make, and in whatever time they take.

# Here are some assumptions:
#
# 1. you must provide a source_db_name, and it must be an 'existing' db or one you have permissions to create
# 2. if that source_db_name == db_name (aka -d or from config file etc) then you MUST have provided at least a different
# source_db_host argument !!
# from there, a more specific `source_` variable will override, or become the currently derived ones.
# e.g. if `source_db_password` is not set, then `db_password` will be used in that context.

# TODO Do I need the guard later if I don't give it a default ?
parser.add_argument('--source_db_name', dest='source_db_name', type=str, default='',
                    help='Postgres DB Name for source of the copy (can be on the same or diffferent host/connection details)')
parser.add_argument('--source_db_host', dest='source_db_host', type=str,
                    help='postges database host')
parser.add_argument('--source_db_port', dest='source_db_port',  type=str,
                    help='postges database host port')
parser.add_argument('--source_db_user', dest='source_db_user', type=str,
                    help='postges database user name')
parser.add_argument('--source_db_password', dest='source_db_password', type=str,
                    help='postges database password')
parser.add_argument('--source_data_dir', dest='source_data_dir', type=str,
                    help='odoo data_dir')
parser.add_argument('--pg_strategy', dest='pg_strategy', type=str, default='fast_copy_template',
                    help='''(skip OR fast_copy_template OR fast_copy OR stream_copy OR custom_dump_copy)
"skip" will do nothing with postgres.

"fast_copy_template" requires the same postgres host for both sides and will require not current users accessing the source database. 
"fast_copy_template" will kill all connections to the database to make the attempt.  Generally this is bad for long running workloads that 
cannot easily be restarted (e.g. something that is communicating with a financial institution and has already committed in the remote but not in Odoo).

"fast_copy" also requries the same postgres host on both sides. This will perform an efficient copy...
"fast_copy" will fail back to custom_dump_copy, as it is the fastest when the dbhost known to be compatible.

"stream_copy" will essentially \'pipe\' the output from a regular dump to a psql on the receiving host.

"custom_dump_copy" will use postgres's major version limited custom dump.  It will also use more than one CPU core to achieve the result.
''')
parser.add_argument('--core_count', dest='core_count', type=str, default='4',
                    help='Core count to use when taking custom dumps. (only for custom format directory dump)')
parser.add_argument('--existing_db', dest='existing_db', type=str, default='',
                    help='behavior if database exists. (delete, skip, halt) (Note that the filestore will '
                         'still be copied if it is exists unless you also use --filestore_strategy skip')

parser.add_argument('--skip_new_database_uuid', dest='skip_new_database_uuid', type=str, default='',
                    help='By default, we will set a new database uuid for subscription reasons. Set to 1 to skip.')
parser.add_argument('--restore_test', dest='restore_test', type=str, default='',
                    help='after database copy, if \'1\' will archive scheduled actions and delete mail servers. '
                         'Otherwise, should be a path to sql object.')
parser.add_argument('--restore_sql', dest='restore_sql', type=str, default='',
                    help='after database copy, sql file to run against it')

parser.add_argument('--filestore_strategy', dest='filestore_strategy', type=str, default='symlink',
                    help='''(skip OR symlink OR copy OR move) "skip" does nothing with the filestore.
"symlink" will recursively make a symlink to every file.  This is good for staging or test copies of a database to save space and time.
It is not particularly durable in that the filestore objects (e.g. sale order PDF) could be deleted (e.g. as a retention policy clean up),
but it is more efficient than a true copy and takes up much less space. "copy" uses the most efficient way to copy the files on disk.

"copy" is durable, but will double the amount of storage required. "move" will essentially \'rename\' the database portion of the filesystem.

"move" is very dangerous and is only appropriate in an extreme time like when doing an upgrade on a VERY large database\'s filestore.
''')

args = parser.parse_args()

# the Odoo standard arguments
db_name, db_user, db_host, db_port, db_password, data_dir = args.db_name, args.db_user, args.db_host, args.db_port, args.db_password, args.data_dir
other_db_name = args.other_db_name
if other_db_name:
    # we have to trust the cli flag users have control over (i.e. not from config file)
    db_name = other_db_name

# source
source_db_name, source_db_user, source_db_host, source_db_port, source_db_password, source_data_dir, core_count, existing_db = args.source_db_name, args.source_db_user, args.source_db_host, args.source_db_port, args.source_db_password, args.source_data_dir, args.core_count, args.existing_db
if not source_db_name:
    raise Exception('--source_db_name is required!')

if not db_name:
    raise Exception('-d OR --db_name')

# pg_strategy tells us some requirements on the need to have credentials for the upstream postgres.
pg_strategy = args.pg_strategy
if not pg_strategy:
    raise Exception('--pg_strategy cannot be empty')

db_done = False
found_database = False
found_database_table_count = 0
if pg_strategy == 'skip':
    db_done = pg_strategy
else:
    print('Attempting connection to inspect existing database...')
    try:
        with db_connect(db_name, db_user, db_password, db_host, db_port) as db:
            db.set_isolation_level(ISOLATION_LEVEL_AUTOCOMMIT)
            with db.cursor() as cr:
                cr.execute('SELECT count(*) FROM pg_class c JOIN pg_namespace s ON s.oid = c.relnamespace '
                           'WHERE s.nspname not in (\'pg_catalog\', \'information_schema\') '
                           'AND s.nspname not like \'pg_temp%\' '
                           'AND s.nspname not like \'pg_toast%\' '
                           'AND c.relname not in (\'pg_stat_statements\') ')
                table_count_res = cr.fetchall()
                found_database = True
                if table_count_res and table_count_res[0]:
                    found_database_table_count = table_count_res[0][0]
    except PGOperationalError:
        print('  connection PGOperationalError, assuming database does not exist.')

if found_database and found_database_table_count:
    if existing_db == 'skip':
        db_done = 'skip_existing_db'
    elif existing_db in ('delete', 'drop'):
        print('Existing database found, dropping...')
        db_drop(db_name, db_user, db_password, db_host, db_port)
    else:
        # halt is an option, but it would be the same as any unknown behavior
        raise Exception('Existing database (with table count %s) found with existing db strategy '
                        '"%s" and copy strategy %s.  Halting!' % (found_database_table_count, existing_db, pg_strategy))

if not db_done and pg_strategy in ('fast_copy_template', 'fast_copy'):
    source_db_host = source_db_host or db_host
    if source_db_host and source_db_host != db_host:
        raise Exception('--pg_strategy %s : requires the same source and destination hosts' % (pg_strategy, ))
    # we do allow new user or 'owner'
    source_db_user = source_db_user or db_user
    source_db_password = source_db_password or db_password
    source_db_port = source_db_port or db_port

    try:
        print('  attempting template based create')
        db_create(db_name, source_db_user, source_db_password, db_host, db_port, template=source_db_name, owner=db_user)
        db_done = pg_strategy
    except PGObjectInUse:
        print('  database is in use!')
        if pg_strategy == 'fast_copy_template':
            print('  killing existing connections and attempting template based create')
            db_create(db_name, source_db_user, source_db_password, db_host, db_port, template=source_db_name,
                      owner=db_user, terminate_connections=True)
            db_done = pg_strategy
# 'fast_copy' can fall down to here...
if not db_done and pg_strategy in ('fast_copy', 'custom_dump_copy'):
    source_db_host = source_db_host or db_host
    source_db_user = source_db_user or db_user
    source_db_password = source_db_password or db_password
    source_db_port = source_db_port or db_port
    dump_dir = '/tmp'  # Good enough?
    dump_path = os.path.join(dump_dir, 'dump')
    cmd = ['pg_dump', '--no-owner']
    cmd += ['-Fd', '-j', core_count, '-f', dump_path, source_db_name]
    print('  dumping database to files with %s cores' % (core_count, ))
    run(cmd, env=exec_pg_environ(source_db_user, source_db_password, source_db_host, source_db_port))
    print('Done dumping.  Restoring!!')
    # try to create, we shouldn't have gotten this far with a db anyway
    # ensure db exists or create
    print('  attempting connection to destination database...')
    try:
        with db_connect(db_name, db_user, db_password, db_host, db_port) as db:
            with db.cursor() as cr:
                pass
    except PGOperationalError:
        print('  attempting creation of database.')
        db_create(db_name, db_user, db_password, db_host, db_port)
    cmd_restore = ['pg_restore', '--no-owner', '-Fd', dump_path, '-d', db_name, '-j', core_count]
    print('running restore, this can give errors and is knkown to halt here, so we will not check the return value of the command')
    run(cmd_restore, env=exec_pg_environ(db_user, db_password, db_host, db_port), check=False)
    db_done = pg_strategy

if not db_done and pg_strategy in ('stream_copy', ):
    source_db_host = source_db_host or db_host
    source_db_user = source_db_user or db_user
    source_db_password = source_db_password or db_password
    source_db_port = source_db_port or db_port
    # ensure db exists or create
    print('  attempting connection to destination database...')
    try:
        with db_connect(db_name, db_user, db_password, db_host, db_port) as db:
            with db.cursor() as cr:
                pass
    except PGOperationalError:
        print('  attempting creation of database.')
        db_create(db_name, db_user, db_password, db_host, db_port)

    print('Opening stream!')
    dump_command = ['pg_dump', '--no-owner', source_db_name]
    dump_process = Popen(dump_command, stdout=PIPE, env=exec_pg_environ(source_db_user, source_db_password, source_db_host, source_db_port))
    rec_command = ['psql', '-d', db_name]
    rec_process = Popen(rec_command, stdin=dump_process.stdout, stdout=PIPE, env=exec_pg_environ(db_user, db_password, db_host, db_port))
    dump_process.stdout.close()
    output = rec_process.communicate()
    # there will be a lot of output
    # print('stream complete output: ' + str(output))
    if dump_process.returncode:
        raise Exception('Dumping process raised error: %s' % (dump_process.returncode, ))
    if rec_process.returncode:
        raise Exception('Receiving process raised error: %s' % (rec_process.returncode, ))
    db_done = pg_strategy

if not db_done:
    exit('DB Operation not resolved as successful.')
print('DB Copied using %s' % (db_done, ))

print('After database copy:')
skip_new_database_uuid = args.skip_new_database_uuid
restore_test = args.restore_test
restore_sql = args.restore_sql
if not skip_new_database_uuid:
    with db_connect(db_name, db_user, db_password, db_host, db_port) as db:
        db.set_isolation_level(ISOLATION_LEVEL_AUTOCOMMIT)
        with db.cursor() as cr:
            uuid = str(uuid.uuid1())
            print('  setting database.uuid=%s' % (uuid, ))
            cr.execute('UPDATE ir_config_parameter SET value = %s WHERE key = %s', (uuid, 'database.uuid'))
if restore_test:
    if restore_test == '1':
        restore_test = '/flow/odoo/restore_test.sql'
    if os.path.isfile(restore_test):
        print('running restore_test sql "%s"' % (restore_test, ))
        cmd = ['psql', '--file=' + restore_test, '-d', db_name]
        run(cmd, env=exec_pg_environ(db_user, db_password, db_host, db_port))
    else:
        print('  cannot find restore_test file %s' % (restore_test, ))

if restore_sql and os.path.isfile(restore_sql):
    print('running restore_sql additional sql file "%s"' % (restore_sql, ))
    cmd = ['psql', '--file=' + restore_sql, '-d', db_name]
    run(cmd, env=exec_pg_environ(db_user, db_password, db_host, db_port))

"""
Filestore
"""
filestore_strategy = args.filestore_strategy
fs_done = False
source_data_dir = source_data_dir or data_dir

if not filestore_strategy or filestore_strategy == 'skip':
    fs_done = 'skip'
else:
    source_path = os.path.join(os.path.join(source_data_dir, 'filestore'), source_db_name)
    if not os.path.exists(source_path) or not os.path.isdir(source_path):
        print('Warning the data directory is missing. %s' % (source_path, ))
        fs_done = 'missing_source'

if not fs_done and filestore_strategy in ('symlink', 'copy', 'move'):
    source_path = os.path.join(os.path.join(source_data_dir, 'filestore'), source_db_name)
    dest_fs = os.path.join(data_dir, 'filestore')
    if not os.path.exists(dest_fs):
        run(['mkdir', '-p', dest_fs])
    dest_path = os.path.join(dest_fs, db_name)
    if filestore_strategy == 'symlink':
        cmd = ['cp', '-as', source_path, dest_path]
    elif filestore_strategy == 'copy':
        cmd = ['cp', '-a', source_path, dest_path]
    elif filestore_strategy == 'move':
        cmd = ['mv', source_path, dest_path]
    print('running: %s' % (' '.join(cmd), ))
    run(cmd)
    fs_done = filestore_strategy

if not fs_done:
        exit('FS Operation not resolved as successful.')
print('FS Copied using %s' % (fs_done,))

exit(0)
