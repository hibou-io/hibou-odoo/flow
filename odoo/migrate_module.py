import os
from flow.odoo.common import parser
from subprocess import run as prun, PIPE
from importlib import import_module


def run(cmd_list, input=None, check=True):
    p = prun(cmd_list, stdout=PIPE, input=input, encoding='utf-8', errors='ignore')
    if check:
        p.check_returncode()
    return p

parser.add_argument('module_name', type=str, help='Module to migrate (e.g. my_web)')
parser.add_argument('from_branch', type=str, help='from Branch (e.g. 12.0)')
parser.add_argument('to_branch', type=str, help='to Branch (e.g. 13.0)')

# Optionals with defaults
parser.add_argument('--work-dir', dest='work_dir', default='/src', help='directory where module exists')
parser.add_argument('--work-dir-dest', dest='work_dir_dest', default=False, help='directory where module will exist (if different, used for extracting module)')
parser.add_argument('--no-git', dest='no_git', default=False, const=True, nargs='?', help='skip git operations')
parser.add_argument('--pull', dest='git_pull', default=False, const=True, nargs='?', help='should git pull on both branches from origin')
parser.add_argument('--commit', dest='git_commit', default=False, const=True, nargs='?', help='should git commit intermediate')
parser.add_argument('--push', dest='git_push', default=False, const=True, nargs='?', help='should git push to origin')


args = parser.parse_args()

# Required
module_name_original, from_branch, to_branch = args.module_name, args.from_branch, args.to_branch
# Optional or has defaults
work_dir, work_dir_dest, no_git, git_pull, git_commit, git_push = args.work_dir, args.work_dir_dest, args.no_git, args.git_pull, args.git_commit, args.git_push


for module_name in module_name_original.split(','):
    # Get into working directory
    os.chdir(work_dir)


    if not no_git:
        # Pull the 'from branch'
        if git_pull:
            run(['git', 'checkout', from_branch])
            run(['git', 'pull'])

        if not work_dir_dest:
            # Checkout and pull 'to branch'
            run(['git', 'checkout', to_branch])
            if git_pull:
                run(['git', 'pull'])
        else:
            os.chdir(work_dir_dest)
            run(['git', 'checkout', to_branch])
            if git_pull:
                run(['git', 'pull'])
            os.chdir(work_dir)

        # when extracting a module, the to_branch will be used
        if not work_dir_dest:
            # Create new branch
            branch_name = 'mig/%s/%s' % (to_branch, module_name)
            p = run(['git', 'checkout', '-b', branch_name], check=False)
            if p.returncode and p.returncode != 128:
                p.check_returncode()  # Raise
            elif p.returncode == 128:
                # Couldn't make branch, but can checkout?
                run(['git', 'checkout', branch_name])
        # git format-patch --keep-subject --stdout origin/13.0..origin/12.0 -- rma_sale | git am -3 --keep
        
        if not work_dir_dest:
            compare_name = '%s..%s' % (to_branch, from_branch)
            p = run(['git', 'format-patch', '--keep-subject', '--stdout', compare_name, '--', module_name])
        else:
            p = run(['git', 'format-patch', '--keep-subject', '--stdout', '--root', from_branch, '--', module_name])
        
        patch = p.stdout
        if work_dir_dest:
            os.chdir(work_dir_dest)
        p = run(['git', 'am', '-3', '--keep'], input=patch, check=False)
        if p.returncode and p.returncode not in (1, 128):
            p.check_returncode()  # Raise
        if p.returncode in (1, 128):
            print('  aborting for %s' % (module_name, ))
            run(['git', 'am', '--abort'])

    if not work_dir_dest:
        # no scripts when extracting to a different repo
        try:
            print('Using: mig.migrate_to')
            migrate = import_module('mig.migrate_to')
            migrate.run(args)
            print('Done: mig.migrate_to')
        except ImportError as e:
            print(e)

        try:
            module_name_under = 'mig.migrate_to_%s' % (to_branch.replace('.', '_'), )
            print('Using: ' + module_name_under)
            migrate = import_module(module_name_under)
            migrate.run(args)
            print('Done: ' + module_name_under)
        except ImportError as e:
            print(e)

    if not no_git:
        # Commit
        if git_commit:
            message = '"MIG `%s` to %s"' % (module_name, to_branch)
            p = run(['git', 'commit', '-a', '-m', message], check=False)
            if p.returncode and p.returncode != 1:
                p.check_returncode()
            if git_push:
                run(['git', 'push', 'origin', branch_name])
