# This is designed to interact with a postgres database with Odoo already installed.
# This is to correct archived views (or other archived models) after Odoo upgrades.
from packaging.version import parse as version_parse

from flow.odoo.common import parser
from flow.tools.postgres import db_connect, ISOLATION_LEVEL_AUTOCOMMIT

parser.add_argument('--behavior', dest='behavior', type=str, default='activate',
                    help='activate (default) or deactivate')

parser.add_argument('--modules', dest='modules', type=str,
                    help='module list (comma separated, e.g. my_mod,some_mod )')

parser.add_argument('--version', dest='version', type=str, default='',
                    help='version to check installed against, leave blank for any')

parser.add_argument('--models', dest='models', type=str, default='ir.ui.view',
                    help='models to re-activ')


args = parser.parse_args()

# the Odoo standard arguments
db_name, db_user, db_host, db_port, db_password, data_dir = args.db_name, args.db_user, args.db_host, args.db_port, args.db_password, args.data_dir
other_db_name = args.other_db_name
if other_db_name:
    # we have to trust the cli flag users have control over (i.e. not from config file)
    db_name = other_db_name

behavior, modules, version, models = args.behavior, args.modules, args.version, args.models

if behavior not in ('activate', 'deactivate'):
    raise ValueError('--behavior (%s) must be (activate OR deactivate)' % (behavior, ))
else:
    active = behavior == 'activate'

if not models:
    raise ValueError('--models is required, leave blank to work on ir.ui.view')

if not version:
    print('No --version was specified, no checks will be made.')
else:
    version = version_parse(version)

modules = modules.split(',')
models = models.split(',')
model_data = {}

with db_connect(db_name, db_user, db_password, db_host, db_port) as db:
    db.set_isolation_level(ISOLATION_LEVEL_AUTOCOMMIT)
    with db.cursor() as cr:
        cr.execute('SELECT id, name, latest_version, state FROM ir_module_module WHERE name in %s', (tuple(modules), ))
        module_data = cr.fetchall()
        for module_row in module_data:
            _id, _name, _latest_version, _state = module_row
            if _state != 'installed':
                print('skipping %s as state is %s' % (_name, _state))
                continue
            if version:
                latest_version = version_parse(_latest_version)
                if latest_version < version:
                    print('skipping %s as version is %s' % (_name, _latest_version))
                    continue
            for model in models:
                if model not in model_data:
                    cr.execute('SELECT id, model, transient FROM ir_model WHERE model = %s', (model, ))
                    _model_data = cr.fetchall()
                    if not _model_data:
                        print('skipping model %s, cannot be found' % (model, ))
                        model_data[model] = False
                        continue

                    model_id, model_model, model_transient = _model_data[0]
                    if model_transient:
                        print('skipping model %s, it is transient')
                        model_data[model] = False
                        continue
                    model_data[model] = model_id

                if not model_data[model]:
                    # we already said why we were skipping
                    continue
                # we can only really guess that they are the default naming convention
                model_table = model.replace('.', '_')
                # need to put the table name into it as it cannot be escaped
                q = '''
UPDATE ''' + model_table + ''' 
SET active = %s 
WHERE id in (SELECT res_id FROM ir_model_data WHERE module = %s AND model = %s)
                '''
                cr.execute(q, (active, _name, model, ))

print('Done!')
