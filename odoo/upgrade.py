import os
import sys
from time import sleep
from subprocess import Popen, PIPE, STDOUT

from flow.odoo.common import parser
from flow.tools.run import run
from flow.tools.postgres import exec_pg_environ

parser.add_argument('--type', dest='upgrade_type', type=str, default='test',
                    help='Upgrade Type e.g. test or production')
parser.add_argument('-t', dest='target_version', type=str, default='',
                    help='Target Version e.g. 14.0')
parser.add_argument('-c', dest='enterprise_code', type=str, default='',
                    help='Enterprise Code')
parser.add_argument('--resume_count', dest='resume_count', type=str, default='5',
                    help='Number of times to try to resume the upgrade if disconnected.')
parser.add_argument('--restore_db_name', dest='restore_db_name', type=str, default='',
                    help='Desired resulting database name.')
parser.add_argument('--restore_sql', dest='restore_sql', type=str, default='',
                    help='after database restore, sql file to run against it')

args = parser.parse_args()

# Note that data_dir is hardcoded in script...
db_name, db_user, db_host, db_port, db_password, data_dir = args.db_name, args.db_user, args.db_host, args.db_port, args.db_password, args.data_dir

other_db_name = args.other_db_name
if other_db_name:
    # we have to trust the cli flag users have control over (i.e. not from config file)
    db_name = other_db_name

# main upgrade arguments
upgrade_type, target_version, enterprise_code, restore_db_name, restore_sql = args.upgrade_type, args.target_version, args.enterprise_code, args.restore_db_name, args.restore_sql
resume_count = int(args.resume_count)

upgrade_requirements = (upgrade_type, target_version)
if not all(upgrade_requirements):
    raise Exception('Upgrade Type, Target Version is required (%s, %s)' % upgrade_requirements)

# setup to run by getting to the scripts directory
flow_odoo_path = os.path.dirname(os.path.realpath(__file__))
os.chdir(os.path.join(flow_odoo_path, 'scripts'))

# Arguments and Environtment to be repeated until proper exit or resume count is depleted
script = ['./upgrade.py', upgrade_type, '-d', db_name, '-t', target_version]
if enterprise_code:
    script.append('-c')
    script.append(enterprise_code)
if restore_db_name:
    script.append('-r')
    script.append(restore_db_name)
environment = exec_pg_environ(db_user, db_password, db_host, db_port)
new_db_name = ''

while resume_count >= 0:
    resume_count -= 1
    upgrade_process = Popen(script, stdin=PIPE, stdout=PIPE, stderr=STDOUT, preexec_fn=os.setpgrp,
                            env=environment)

    line = ''
    for line in iter(upgrade_process.stdout.readline, b''):
        line = line.decode()
        sys.stdout.write(line)
        if line.find('This upgrade request seems to have been interrupted. Do you want to resume it ? [Y/n]') >= 0:
            print('FOUND RESUME')
            upgrade_process.stdin.write('Y\n'.encode())
            upgrade_process.stdin.flush()
        if line.find('Restore the dump file \'upgraded.dump\' as the database \'') >= 0:
            new_db_name = line.split("'")[3]

    if line.find('Error: The \'rsync\' command has failed') >= 0:
        print('found RSYNC ERROR, sleeping 5')
        sleep(5)
        pass
    if line.find('UnicodeDecodeError:') >= 0:
        print('found UnicodeDecodeError, sleeping 5')
        sleep(5)
        pass
    elif not upgrade_process.returncode:
        if new_db_name:
            print('RESTORED AS ' + new_db_name)
            if restore_sql and os.path.isfile(restore_sql):
                print('running restore_sql additional sql file "%s"' % (restore_sql,))
                args = ['psql', '--file=' + restore_sql, '-d', new_db_name]
                run(args, env=environment)
        exit(0)
    else:
        # this is probably not possible...
        print('EXIT CODE "%s"' % (upgrade_process.returncode, ))

# Exits because the resume count was depleted.
# This CAN mean that a controlling process starts this process again.
# This may mean that we are more tolerant of rsync issues or if the script actually exits with non-zero
# As an example, kubernetes jobs may want to be setup to not restart on failure
exit(-1)
