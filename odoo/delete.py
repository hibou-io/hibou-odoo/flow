import os

from flow.tools.run import run
from flow.tools.postgres import db_drop, db_find, db_rename

from flow.odoo.common import parser

parser.add_argument('--pg_strategy', dest='pg_strategy', type=str, default='',
                    help='''(skip OR delete OR delete-later-rename OR do-delete-later)
Default will be "delete" if db_name is not DO_ALL, when it will default to "do-delete-later".
"skip" will do nothing with postgres.

"delete" will kill all connections to that database and drop the database. (delete all data) 
This is permanent outside of some external backup, or snapshot of the postgres server itself.

"delete-later-rename" will kill all connections to the database and rename it with "flow-delete-later." in front of it.
This should only be used in situations where the db needs inspected or it is "assumed" to not be needed later.
E.g. a very large production database that just went through a full copy during upgrade.

"do-delete-later" used to actually drop the "flow-delete-later." databases.  Run as a cron, or when you are ready to 
actually delete the database.  This command allows the db_name to end up being 'DO_ALL' to let the command try to drop 
'all' of the pattern databases. 
''')

parser.add_argument('--filestore_strategy', dest='filestore_strategy', type=str, default='',
                    help='''(skip OR delete OR delete-later-rename OR do-delete-later)
Default will be "delete" if db_name is not DO_ALL, when it will default to "do-delete-later".

"skip" will do nothing with the filestore.

"delete" deletes the filestore. This is permanent! (delete all data)

"delete-later-rename" will rename the filestore with "flow-delete-later." in front of it.
This should only be used in situations where the fs needs inspected or it is "assumed" to not be needed later.
E.g. a very large production database that just went through a full filestore copy during an upgrade.

"do-delete-later" used to actually delete the "flow-delete-later." filestores.  Run as a cron, or when you are ready to 
actually delete the filestore.  This command allows the db_name to end up being 'DO_ALL' to let the command try to delete 
'all' of the pattern filestores. 
''')

args = parser.parse_args()

DO_ALL = 'DO_ALL'
DELETE_LATER_PREFIX = 'flow-delete-later.'

# the Odoo standard arguments
db_name, db_user, db_host, db_port, db_password, data_dir = args.db_name, args.db_user, args.db_host, args.db_port, args.db_password, args.data_dir
other_db_name = args.other_db_name
if other_db_name:
    # we have to trust the cli flag users have control over (i.e. not from config file)
    db_name = other_db_name

pg_strategy = args.pg_strategy
if not pg_strategy:
    pg_strategy = 'do-delete-later' if db_name == DO_ALL else 'delete'

if not db_name:
    raise Exception('db_name required, usually via flag -d or config file.')

if not pg_strategy:
    raise Exception('--pg_strategy is required, maybe use \'skip\' (default is \'delete\')?')

pg_done = False
if pg_strategy == 'skip':
    pg_done = pg_strategy

if db_name == DO_ALL and pg_strategy != 'do-delete-later':
    raise Exception('Illegal db_name %s without using --pg_strategy do-delete-later' % (DO_ALL, ))

databases_to_drop = []

if not pg_done and pg_strategy == 'do-delete-later':
    if db_name == DO_ALL:
        # find 'drop-
        databases_to_drop += db_find(db_user, db_password, db_host, db_port, prefix=DELETE_LATER_PREFIX)
        if not databases_to_drop:
            print('found no databases with prefix %s' % (DELETE_LATER_PREFIX, ))
    else:
        search_db_name = db_name
        if not db_name.startswith(DELETE_LATER_PREFIX):
            search_db_name = DELETE_LATER_PREFIX + db_name
        databases_to_drop += db_find(db_user, db_password, db_host, db_port, db_name=search_db_name)
        if not databases_to_drop:
            print('found no databases named %s' % (search_db_name, ))
    if databases_to_drop:
        print('found databases to drop %s' % (databases_to_drop, ))
    else:
        pg_done = 'not found'

    # Note that this just lines up work for the drop to do later...

if not pg_done and (pg_strategy == 'delete' or databases_to_drop):
    if not databases_to_drop and db_name:
        databases_to_drop += db_find(db_user, db_password, db_host, db_port, db_name=db_name)
    if not databases_to_drop:
        print('No databases found to drop given %s, %s' % (pg_strategy, db_name))
        pg_done = 'not found'
    else:
        for drop_db_name in databases_to_drop:
            print('  dropping... %s' % (drop_db_name, ))
            db_drop(drop_db_name, db_user, db_password, db_host, db_port)
        pg_done = 'delete'

if not pg_done and pg_strategy == 'delete-later-rename':
    db_exists = db_find(db_user, db_password, db_host, db_port, db_name=db_name)
    if not db_exists:
        print('No databases found to delete-later-rename given %s' % (db_name, ))
        pg_done = 'not found'
    else:
        dest_db_name = DELETE_LATER_PREFIX + db_name
        db_rename(db_name, dest_db_name, db_user, db_password, db_host, db_port)
        pg_done = pg_strategy

if not pg_done:
    exit('DB Operation not resolved as successful.')
print('DB Dropped using %s' % (pg_done, ))

"""
Filestore
"""
filestore_strategy = args.filestore_strategy
if not filestore_strategy:
    print('getting default fs ')
    filestore_strategy = 'do-delete-later' if db_name == DO_ALL else 'delete'
    print('  ' + str(filestore_strategy))

fs_done = False

if filestore_strategy == 'skip':
    fs_done = 'skip'
else:
    fs_path = os.path.join(data_dir, 'filestore')
    if not os.path.exists(fs_path) or not os.path.isdir(fs_path):
        print('Warning the data directory is missing. %s' % (fs_path, ))
        fs_done = 'missing filestore'


def find_filestores(fs_name=None, prefix=None):
    if fs_name:
        maybe_fs_path = os.path.join(fs_path, fs_name)
        if os.path.exists(maybe_fs_path) and os.path.isdir(maybe_fs_path):
            return [maybe_fs_path]
    elif prefix:
        for root, dirs, files in os.walk(fs_path):
            return [name for name in dirs if name.startswith(prefix)]
    return []


if db_name == DO_ALL and filestore_strategy != 'do-delete-later':
    raise Exception('Illegal db_name %s without using --filestore_strategy do-delete-later' % (DO_ALL, ))

filestores_to_delete = []
if not fs_done and filestore_strategy == 'do-delete-later':
    if db_name == DO_ALL:
        filestores_to_delete += find_filestores(prefix=DELETE_LATER_PREFIX)
    else:
        search_fs_name = db_name
        if not db_name.startswith(DELETE_LATER_PREFIX):
            search_fs_name = DELETE_LATER_PREFIX + db_name
        filestores_to_delete += find_filestores(fs_name=search_fs_name)
    if filestores_to_delete:
        print('found filestores to delete %s' % (filestores_to_delete, ))
    else:
        fs_done = 'not found'

if not fs_done and (filestore_strategy == 'delete' or filestores_to_delete):
    if not filestores_to_delete:
        filestores_to_delete += find_filestores(fs_name=db_name)
    if not filestores_to_delete:
        print('No filestores found to delete given %s, %s' % (filestore_strategy, db_name))
        fs_done = 'not found'
    else:
        for delete_fs_name in filestores_to_delete:
            delete_fs_path = os.path.join(fs_path, delete_fs_name)
            print('  deleting... %s' % (delete_fs_path, ))
            run(['rm', '-rf', delete_fs_path])
        fs_done = 'delete'

if not fs_done and filestore_strategy == 'delete-later-rename':
    fs_exists = find_filestores(fs_name=db_name)
    if not fs_exists:
        print('No filestore found to delete-later-rename given %s' % (db_name, ))
        fs_done = 'not found'
    else:
        src_fs_path = fs_exists[0]
        dest_fs_name = DELETE_LATER_PREFIX + db_name
        dest_fs_path = os.path.join(fs_path, dest_fs_name)
        run(['mv', src_fs_path, dest_fs_path])
        fs_done = filestore_strategy

if not fs_done:
    exit('FS Operation not resolved as successful.')
print('FS Deleted using %s' % (fs_done, ))

# TODO Some day need to delete minio databases?

exit(0)
