import os

from flow.odoo.common import parser
from flow.tools.run import run
from flow.tools.postgres import db_connect, db_create, db_drop, exec_pg_environ, ISOLATION_LEVEL_AUTOCOMMIT, PGOperationalError

parser.add_argument('--file', dest='file', type=str, default='',
                    help='SQL file to apply on database.')


args = parser.parse_args()

# the Odoo standard arguments
db_name, db_user, db_host, db_port, db_password, data_dir = args.db_name, args.db_user, args.db_host, args.db_port, args.db_password, args.data_dir
other_db_name = args.other_db_name
if other_db_name:
    # we have to trust the cli flag users have control over (i.e. not from config file)
    db_name = other_db_name

file = args.file
if not file:
    raise ValueError('--file is required, and will be stdin for psql')

if file and os.path.isfile(file):
    environment = exec_pg_environ(db_user, db_password, db_host, db_port)
    print('running sql file "%s"' % (file, ))
    args = ['psql', '--file=' + file, '-d', db_name]
    run(args, env=environment)

print('Done!')
