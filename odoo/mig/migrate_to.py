import os


def run(args):
    work_dir, module_name, from_branch, to_branch = args.work_dir, args.module_name, args.from_branch, args.to_branch
    path = '%s/%s' % (work_dir, module_name)
    walk_pieces = [x for x in os.walk(path)]
    files = walk_pieces[0][2]
    if '__init__.py' in files and ('__manifest__.py' in files or '__openerp__.py' in files):
        if '__manifest__.py' in files:
            file_name = '__manifest__.py'
        else:
            file_name = '__openerp__.py'
        with open(path + '/' + file_name, 'r') as manifest:
            manifest_data = manifest.read()
        # Patch Manifest Data
        manifest_data = manifest_data.replace(from_branch, to_branch)
        with open(path + '/' + file_name, 'w') as manifest:
            manifest.write(manifest_data)
