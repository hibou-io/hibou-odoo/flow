import os


def run(args):
    work_dir, module_name = args.work_dir, args.module_name
    path = '%s/%s' % (work_dir, module_name)
    walk_pieces = [x for x in os.walk(path)]
    files = []
    for p in walk_pieces:
        if len(p) >= 3:
            files += [p[0] + '/' + t for t in p[2]]

    for fname in files:
        if fname.endswith('.py'):
            with open(fname, 'r') as f:
                f_data = f.read()
            f_data = f_data.replace('address_home_id', 'work_contact_id')
            f_data = f_data.replace('l10n_generic_coa', 'account')
            f_data = f_data.replace('states=', '\nstates=')
            f_data = f_data.replace('readonly=1', 'readonly=True')
            f_data = f_data.replace('required=1', 'required=True')
            f_data = f_data.replace('readonly=0', 'readonly=False')
            f_data = f_data.replace('required=0', 'required=False')
            with open(fname, 'w') as f:
                f.write(f_data)
        if fname.endswith('.xml'):
            with open(fname, 'r') as f:
                f_data = f.read()
            f_data = f_data.replace('address_home_id', 'work_contact_id')
            f_data = f_data.replace('attrs="', '\nattrs="')
            f_data = f_data.replace('states="', '\nstates="')
            f_data = f_data.replace('l10n_generic_coa', 'account')
            f_data = f_data.replace('//div[@data-key=', '//app[@name=')
            with open(fname, 'w') as f:
                f.write(f_data)
