#!/usr/bin/env python

# designed to run in flow
import os

import sys
sys.path.append(os.path.sep)

from subprocess import Popen, PIPE

from flow.tools.run import run

from flow.odoo.common import parser

parser.add_argument('--user', dest='odoo_user', type=str, default='admin',
                    help='Odoo User Login name (for requests)')
parser.add_argument('--pass', dest='odoo_pass', type=str, default='admin',
                    help='Odoo User Login password (for requests)')
parser.add_argument('--host', dest='odoo_host', type=str, default='localhost',
                    help='Odoo Host (for requests)')
parser.add_argument('--port', dest='odoo_port', type=str, default='8069',
                    help='Odoo Port (for requests)')
parser.add_argument('--proto', dest='odoo_proto', type=str, default='jsonrpc',
                    help='Odoo Protocol (for requests)')
parser.add_argument('-f', dest='odoo_locust', type=str, default='./src',
                    help='Odoo Locust file(s)')

args = parser.parse_args()

db_name, odoo_user, odoo_pass, odoo_port, odoo_host, odoo_proto, odoo_locust = args.db_name, args.odoo_user, args.odoo_pass, args.odoo_port, args.odoo_host, args.odoo_proto, args.odoo_locust
other_db_name = args.other_db_name
if other_db_name:
    # we have to trust the cli flag users have control over (i.e. not from config file)
    db_name = other_db_name

if not db_name:
    db_name = os.environ.get('DB_NAME')
    if not db_name:
        raise Exception('-d OR --db_name or env DB_NAME')

env = os.environ.copy()
env['DB_NAME'] = db_name
env['ODOO_USER'] = odoo_user
env['ODOO_PASS'] = odoo_pass
env['ODOO_HOST'] = odoo_host
env['ODOO_PORT'] = odoo_port
env['ODOO_PROTO'] = odoo_proto
env['ODOO_LOCUST'] = odoo_locust

abspath = os.path.abspath(__file__)
dname = os.path.dirname(abspath)
os.chdir(dname)

run([dname +'/entrypoint.sh'], env=env)
