import os
from locust import task, between, TaskSet
from OdooLocust.OdooLocustUser import OdooLocustUser


class PurchaseUser(OdooLocustUser):
    # These tasks are known to work with `sale,purchase` and demo data on Odoo 15.0 CE and EE
    wait_time = between(0.1*2, 10*2)
    database = os.environ.get('DB_NAME', 'odoo')
    login = os.environ.get('ODOO_USER', 'admin')
    password = os.environ.get('ODOO_PASS', 'admin')
    port = int(os.environ.get('ODOO_PORT', 8069))
    host = os.environ.get('ODOO_HOST', 'localhost')
    protocol = os.environ.get('ODOO_PROTO', 'jsonrpc')

    @task
    class BuyerTaskSet(TaskSet):
        @task(10*2)
        def read_partners(self):
            cust_model = self.client.get_model('res.partner')
            cust_ids = cust_model.search([])
            prtns = cust_model.read(cust_ids)

        @task(5*2)
        def read_products(self):
            prod_model = self.client.get_model('product.product')
            ids = prod_model.search([])
            prods = prod_model.read(ids)

        @task(20*2)
        def create_po(self):
            prod_model = self.client.get_model('product.product')
            vend_model = self.client.get_model('res.partner')
            po_model = self.client.get_model('purchase.order')

            vend_id = vend_model.search([('name', 'ilike', 'azure')])[0]
            prod_ids = prod_model.search([('name', 'ilike', 'desk')])

            order_id = po_model.create({
                'partner_id': vend_id,
                'order_line': [(0, 0, {'product_id': prod_ids[0],
                                    'product_uom_qty': 1}),
                            (0, 0, {'product_id': prod_ids[1],
                                    'product_uom_qty': 2}),
                            ]
            })
            po_model.button_confirm([order_id])
