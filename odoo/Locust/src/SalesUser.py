import os
from locust import task, between, TaskSet
from OdooLocust.OdooLocustUser import OdooLocustUser


class SalesUser(OdooLocustUser):
    # These tasks are known to work with `sale,purchase` and demo data on Odoo 15.0 CE and EE
    wait_time = between(0.1, 10)
    database = os.environ.get('DB_NAME', 'odoo')
    login = os.environ.get('ODOO_USER', 'admin')
    password = os.environ.get('ODOO_PASS', 'admin')
    port = int(os.environ.get('ODOO_PORT', 8069))
    host = os.environ.get('ODOO_HOST', 'localhost')
    protocol = os.environ.get('ODOO_PROTO', 'jsonrpc')
    
    @task
    class SellerTaskSet(TaskSet):
        @task(10)
        def read_partners(self):
            cust_model = self.client.get_model('res.partner')
            cust_ids = cust_model.search([])
            prtns = cust_model.read(cust_ids)

        @task(5)
        def read_products(self):
            prod_model = self.client.get_model('product.product')
            ids = prod_model.search([])
            prods = prod_model.read(ids)

        @task(20)
        def create_so(self):
            prod_model = self.client.get_model('product.product')
            cust_model = self.client.get_model('res.partner')
            so_model = self.client.get_model('sale.order')

            cust_id = cust_model.search([('name', 'ilike', 'azure')])[0]
            prod_ids = prod_model.search([('name', 'ilike', 'desk')])

            order_id = so_model.create({
                'partner_id': cust_id,
                'order_line': [(0, 0, {'product_id': prod_ids[0],
                                    'product_uom_qty': 1}),
                            (0, 0, {'product_id': prod_ids[1],
                                    'product_uom_qty': 2}),
                            ]
            })
            so_model.action_confirm([order_id])
