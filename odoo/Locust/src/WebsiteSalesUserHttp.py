import os
from locust import task, between, TaskSet, HttpUser


class WebsiteSalesUserHttp(HttpUser):
    # These tasks are known to work with `sale,purchase` and demo data on Odoo 15.0 CE and EE
    wait_time = between(0.1, 10)
    database = os.environ.get('DB_NAME', 'odoo')
    login = os.environ.get('ODOO_USER', 'admin')
    password = os.environ.get('ODOO_PASS', 'admin')
    port = int(os.environ.get('ODOO_PORT', 8069))
    host = os.environ.get('ODOO_PROTO', 'http') + '://' + os.environ.get('ODOO_HOST', 'localhost')
    protocol = os.environ.get('ODOO_PROTO', 'http')
    
    @task
    class WebsiteUserTaskSet(TaskSet):

        @task(1)
        def home(self):
            home = self.client.get("/")
            logo = self.client.get("/web/image/website/1/logo/300x300?unique=9621b43")
            # TODO need to dynamically get this data from home result
            assets = self.client.get("/web/assets/576-fa76e41/1/web.assets_frontend.min.css")
        
        @task(2)
        def shop(self):
            self.client.get("/shop")
