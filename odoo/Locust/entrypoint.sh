#!/bin/bash

set -e

if [ -z "$(ls -A .env)" ]
then
   echo 'Initializing virtual environment for OdooLocust'
   python -m venv .env
   source .env/bin/activate
   echo 'Installing OdooLocust'
   pip install OdooLocust
fi
source .env/bin/activate

locust --class-picker -f $ODOO_LOCUST
