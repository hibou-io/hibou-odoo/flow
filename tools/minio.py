import re
from minio import Minio
from minio.error import NoSuchUpload

try:
    from minio.sse import SseCustomerKey as MinioSseCustomerKey
except ImportError:
    from minio.sse import SSE_C as MinioSseCustomerKey
try:
    from minio.error import NoSuchKey as MinioNoSuchKey
except ImportError:
    from minio.error import S3Error as MinioNoSuchKey


_s3_uri_re = re.compile("^s3:///*([^/]*)/?(.*)", re.IGNORECASE | re.UNICODE)


def s3_uri(uri):
    match = _s3_uri_re.match(uri)
    if not match:
        raise ValueError("%s: is not a valid S3 URI" % (uri, ))
    return match.groups()


def minio_client(minio_host, minio_region, minio_access_key, minio_secret_key, minio_secure):
    if not all((minio_host, minio_access_key, minio_secret_key)):
        raise Exception('Incorrect configuration of minio client.')
    return Minio(minio_host,
                 access_key=minio_access_key,
                 secret_key=minio_secret_key,
                 region=minio_region,
                 secure=bool(minio_secure))
