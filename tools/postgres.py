import os
import psycopg2
from psycopg2.extensions import ISOLATION_LEVEL_AUTOCOMMIT

PGOperationalError = psycopg2.OperationalError
try:
    from psycopg2.errors import ObjectInUse as PGObjectInUse
except ImportError:
    PGObjectInUse = PGOperationalError


def exec_pg_environ(db_user, db_password, db_host, db_port):
    env = os.environ.copy()
    env['PGHOST'] = db_host
    env['PGPORT'] = db_port
    env['PGUSER'] = db_user
    env['PGPASSWORD'] = db_password
    return env


def db_connect(db_name, db_user, db_password, db_host, db_port):
    return psycopg2.connect(database=db_name, user=db_user, password=db_password, host=db_host, port=db_port)


def _check_db_field_value(value):
    if value.find('"') >= 0:
        raise Exception('Cannot create/drop database named/template/owner "%s"' % (value, ))


def db_create(db_name, db_user, db_password, db_host, db_port, template=None, owner=None, terminate_connections=False):
    _check_db_field_value(db_name)
    if template:
        _check_db_field_value(template)
    if owner:
        _check_db_field_value(owner)
    db = db_connect('postgres', db_user, db_password, db_host, db_port)
    db.set_isolation_level(ISOLATION_LEVEL_AUTOCOMMIT)
    with db.cursor() as cr:
        q = 'CREATE DATABASE "%s"' % (db_name, )
        if template or owner:
            q += '\nWITH '
        if template:
            q += ' TEMPLATE "%s" ' % (template, )
            if terminate_connections:
                _terminate_connections(cr, template)
        if owner:
            q += ' OWNER "%s" ' % (owner, )
        cr.execute(q)  # note not regular escaping


def _terminate_connections(cr, db_name):
    cr.execute('SELECT pg_terminate_backend(pid) FROM pg_stat_get_activity(NULL::integer) WHERE '
               'datid = (SELECT oid FROM pg_database WHERE datname = %s)', (db_name, ))


def db_drop(db_name, db_user, db_password, db_host, db_port):
    _check_db_field_value(db_name)
    db = db_connect('postgres', db_user, db_password, db_host, db_port)
    db.set_isolation_level(ISOLATION_LEVEL_AUTOCOMMIT)
    with db.cursor() as cr:
        # typical way to try to drop database when there may be existing connections
        _terminate_connections(cr, db_name)
        cr.execute('DROP DATABASE "%s"' % (db_name, ))  # note not regular escaping


def db_rename(db_name, dest_db_name, db_user, db_password, db_host, db_port):
    _check_db_field_value(db_name)
    _check_db_field_value(dest_db_name)
    db = db_connect('postgres', db_user, db_password, db_host, db_port)
    db.set_isolation_level(ISOLATION_LEVEL_AUTOCOMMIT)
    with db.cursor() as cr:
        # typical way to try to drop database when there may be existing connections
        _terminate_connections(cr, db_name)
        cr.execute('ALTER DATABASE "%s" RENAME TO "%s";' % (db_name, dest_db_name))  # note not regular escaping


def db_find(db_user, db_password, db_host, db_port, db_name=None, prefix=None):
    if db_name:
        _check_db_field_value(db_name)
    if prefix:
        _check_db_field_value(prefix)

    if not any((db_name, prefix)) or all((db_name, prefix)):
        raise Exception('Call Error: Must have either db_name OR prefix. (db_name=%s, prefix=%s)' % (db_name, prefix))
    res = []
    db = db_connect('postgres', db_user, db_password, db_host, db_port)
    db.set_isolation_level(ISOLATION_LEVEL_AUTOCOMMIT)
    with db.cursor() as cr:
        where = 'datname = %s' if db_name else 'datname like %s'
        vars = (db_name, ) if db_name else (prefix + '%', )
        q = '''
        SELECT datname 
        FROM pg_database 
        WHERE 
        %s;
                    ''' % where
        cr.execute(q, vars)
        q_result = cr.fetchall()
        res = [i[0] for i in q_result]
    return res
