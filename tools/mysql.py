import os
import MySQLdb
# For exporting
from MySQLdb import ProgrammingError as MySQLProgrammingError


def exec_mysql_environ(db_user, db_password, db_host, db_port):
    env = os.environ.copy()
    # different MYSQL versions, and apps (mysqldump vs mysql) may have different
    # environment variables
    env['MYSQL_HOST'] = db_host
    env['MYSQL_PORT'] = db_port
    try:
        env['MYSQL_TCP_PORT'] = str(int(db_port))
        env['MYSQL_UNIX_PORT'] = ''
    except:
        env['MYSQL_TCP_PORT'] = ''
        env['MYSQL_UNIX_PORT'] = db_port
    # environment may have USER from postgres, so overwrite it
    env['USER'] = db_user
    env['MYSQL_USER'] = db_user
    # Deprecated...
    # env['MYSQL_PWD'] = db_password
    return env


def db_connect(db_name, db_user, db_password, db_host, db_port):
    # TODO how to handle socket?  connect expects integer
    return MySQLdb.connect(db_host, db_user, db_password, db_name, int(db_port))
