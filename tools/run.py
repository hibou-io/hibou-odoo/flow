import os
from subprocess import run as prun, PIPE, CalledProcessError


def run(cmd_list, input=None, check=True, stdout=PIPE, env=None):
    if env is None:
        env = os.environ
    p = prun(cmd_list, stdout=stdout, input=input, encoding='utf-8', errors='ignore', env=env)
    if check:
        p.check_returncode()
    return p
