import json
import tempfile
import os
import secrets
import base64
import shutil

from flow.tools.run import run, CalledProcessError
from flow.tools.minio import s3_uri, minio_client, MinioSseCustomerKey
from flow.tools.mysql import db_connect, exec_mysql_environ
# from flow.tools.postgres import db_connect, exec_pg_environ

from flow.magento.common import parser, dump_db_manifest


parser.add_argument('--backup_file', dest='backup_file', type=str, default='',
                    help='location to backup to')
parser.add_argument('--backup_format', dest='backup_format', type=str, default='',
                    help='file format to backup to')
parser.add_argument('--existing_file', dest='existing_file', type=str, default='delete',
                    help='behavior if the backup_file already exists (delete, skip, halt)')
parser.add_argument('--finished_file', dest='finished_file', type=str, default='',
                    help='behavior of backup_file after operation complete (keep, delete)')
parser.add_argument('--skip_db', dest='skip_db', type=str, default='',
                    help='skip backing/dumping up the database')
parser.add_argument('--skip_fs', dest='skip_fs', type=str, default='',
                    help='skip backing the filestore')
# TODO mysqlpump --default-parallelism=4
# parser.add_argument('--core_count', dest='core_count', type=str, default='4',
#                     help='Core count to use when taking custom dumps. (only for custom format directory dump)')

# Destination Arguments
parser.add_argument('--backup_dest', dest='backup_dest', type=str, default='',
                    help='destination of backup (s3:// url, upgrade)')
parser.add_argument('--backup_minio_host', dest='backup_minio_host', type=str, default='',
                    help='when using s3:// dest, S3 host')
parser.add_argument('--backup_minio_port', dest='backup_minio_port', type=str, default='',
                    help='when using s3:// dest, S3 host port')
parser.add_argument('--backup_minio_region', dest='backup_minio_region', type=str, default='us-west-1',
                    help='when using s3:// dest, S3 region')
parser.add_argument('--backup_minio_access_key', dest='backup_minio_access_key', type=str, default='',
                    help='when using s3:// dest, S3 access key')
parser.add_argument('--backup_minio_secret_key', dest='backup_minio_secret_key', type=str, default='',
                    help='when using s3:// dest, S3 secret key')
parser.add_argument('--backup_minio_secure', dest='backup_minio_secure', type=str, default='',
                    help='when using s3:// dest, S3 over https')
parser.add_argument('--backup_minio_sse', dest='backup_minio_sse', type=str, default=None,
                    help='when using s3:// dest, consumer encryption key (32 char of base64)')
args = parser.parse_args()

# the magento standard arguments
db_name, db_user, db_host, db_port, db_password, data_dir = args.db_name, args.db_user, args.db_host, args.db_port, args.db_password, args.data_dir


# my new ones
backup_file, backup_format, existing_file, finished_file, skip_db, skip_fs = args.backup_file, args.backup_format, args.existing_file, args.finished_file, args.skip_db, args.skip_fs
# destination
backup_dest = args.backup_dest
backup_minio_host, backup_minio_port, backup_minio_region, backup_minio_access_key, backup_minio_secret_key, backup_minio_secure, backup_minio_sse = args.backup_minio_host, args.backup_minio_port, args.backup_minio_region, args.backup_minio_access_key, args.backup_minio_secret_key, args.backup_minio_secure, args.backup_minio_sse

if not backup_format and not backup_file:
    # the true defaults
    backup_file = 'backup'
    backup_format = 'zip'
elif not backup_format:
    if backup_file.endswith('.zip'):
        backup_format = 'zip'
    else:
        raise Exception('backup_format not detected in backup_file %s' % (backup_file, ))

if not backup_file:
    # Note we don't need zip on the end as shutil will do that
    backup_file = 'backup'

if backup_file.find('/') < 0:
    backup_file = '/tmp/' + backup_file

# in case someone added extension by hand
backup_file = backup_file.replace('.' + backup_format, '')
# build expected file name
backup_file_real = backup_file + '.' + backup_format

# Defaults for what to do with the file we created
if not backup_dest and not finished_file:
    finished_file = 'keep'
if backup_dest and not finished_file:
    finished_file = 'delete'

# If we have the file and want to delete it first
if os.path.exists(backup_file_real) and existing_file == 'delete':
    print('Deleting existing backup.')
    if os.path.isdir(backup_file_real):
        shutil.rmtree(backup_file_real)
    else:
        os.remove(backup_file_real)

# If we have the file and cannot skip
if os.path.exists(backup_file_real) and existing_file != 'skip':
    raise Exception('backup_file exists (%s) and we are not allowed to skip it. '
                    'Use "--existing_file delete" to delete it. '
                    'Use "--existing_file skip" to skip it (e.g. to upload it)' % (backup_file_real, ))

if not os.path.exists(backup_file_real):
    print('Starting backup to "%s"' % (backup_file_real))
    if backup_format in ('zip', ):
        # Build an Magento compliant zip with filestore
        cmd = ['mysqldump', '--single-transaction=TRUE']
        with tempfile.TemporaryDirectory() as dump_dir:
            if not skip_db:
                print('  creating manifest.json')
                with open(os.path.join(dump_dir, 'manifest.json'), 'w') as fh:
                    db = db_connect(db_name, db_user, db_password, db_host, db_port)
                    with db.cursor() as cr:
                        json.dump(dump_db_manifest(cr, db_name=db_name), fh, indent=4)
            cmd += [
                '--port', db_port,
                '-p' + db_password,  # single arg
                '-h', db_host,
                '-u', db_user,
                '--result-file=' + os.path.join(dump_dir, 'dump.sql'),  # single arg
                db_name,  # single/last arg is db to copy
            ]
            if not skip_db:
                print('  dumping database')
                run(cmd, env=exec_mysql_environ(db_user, db_password, db_host, db_port))

            if data_dir and not skip_fs:
                filestore = data_dir
            else:
                filestore = False

            if filestore and os.path.exists(filestore):
                print('  copying filestore')
                # TODO
                # I really feel like we should be able to skip the copy here and add
                # the directory into the ZIP file created, but
                shutil.copytree(filestore, os.path.join(dump_dir, 'filestore'))
            archive_name = backup_file
            shutil.make_archive(archive_name, 'zip', dump_dir)
    else:
        exit(1)

if not os.path.exists(backup_file_real):
    raise Exception('final backup_file (%s) missing!' % (backup_file_real, ))

if backup_dest:
    # Is S3?
    s3_bucket, s3_item = None, None
    try:
        s3_bucket, s3_item = s3_uri(backup_dest)
    except ValueError:
        pass
    if s3_bucket and s3_item:
        print('Uploading with minio to S3 URL: %s' % (backup_dest, ))
        if backup_minio_port:
            backup_minio_host = backup_minio_host + ':' + backup_minio_port
        client = minio_client(backup_minio_host, backup_minio_region, backup_minio_access_key, backup_minio_secret_key, backup_minio_secure)
        if backup_minio_sse is not None:
            if backup_minio_sse == 'generate':
                backup_minio_sse = base64.b64encode(secrets.token_bytes(32))
                backup_minio_sse = backup_minio_sse[:32]
                print('Generated SSE-C: ' + str(backup_minio_sse.decode()))
            backup_minio_sse = MinioSseCustomerKey(backup_minio_sse)
        if not client.bucket_exists(s3_bucket):
            region = backup_minio_region
            print('  creating bucket %s using region %s' % (s3_bucket, region))
            client.make_bucket(s3_bucket, region)
            if backup_minio_sse:
                config = {
                    'ServerSideEncryptionConfiguration': {
                        'Rule': [
                            {'ApplyServerSideEncryptionByDefault': {'SSEAlgorithm': 'AES256'}}
                        ]
                    }
                }
                client.put_bucket_encryption(s3_bucket, config)
        metadata = None
        if not skip_db:
            print('  generating manifest.json for metadata')
            db = db_connect(db_name, db_user, db_password, db_host, db_port)
            with db.cursor() as cr:
                metadata = dump_db_manifest(cr, db_name=db_name)
        if os.path.isdir(backup_file_real):
            for root, dirs, files in os.walk(backup_file_real):
                for file in files:
                    client.fput_object(s3_bucket, s3_item + '/' + file, os.path.join(backup_file_real, file), sse=backup_minio_sse)
        else:
            client.fput_object(s3_bucket, s3_item, backup_file_real, sse=backup_minio_sse)
            print('Finished Minio Upload.  Retrieve at: ' + str(client.presigned_get_object(s3_bucket, s3_item)))
    elif backup_dest == 'upgrade':
        raise NotImplementedError('upgrade not supported (yet)')
    else:
        raise Exception('Unknown how to deliver to %s' % (backup_dest, ))

# Finalize
if os.path.exists(backup_file_real) and finished_file == 'delete':
    print('Cleaning up')
    if os.path.isdir(backup_file_real):
        shutil.rmtree(backup_file_real)
    else:
        os.remove(backup_file_real)
print('Done')
