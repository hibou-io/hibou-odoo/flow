from flow.tools.args import ArgumentParser
from flow.tools.mysql import MySQLProgrammingError


parser = ArgumentParser()

parser.add_argument('--db_name', dest='db_name', type=str,
                    help='mysql database name')
parser.add_argument('--db_host', dest='db_host', type=str,
                    help='mysql database host')
parser.add_argument('--db_port', dest='db_port',  type=str,
                    help='mysql database host port')
parser.add_argument('--db_user', dest='db_user', type=str,
                    help='mysql database user name')
parser.add_argument('--db_password', dest='db_password', type=str,
                    help='mysql database password')
parser.add_argument('--data_dir', dest='data_dir', type=str,
                    help='mysql data_dir')


def dump_db_manifest(cr, db_name='magento'):
    mysql_version = "%d.%d" % cr.connection._server_version
    # TODO get M1 and M2 versions from database...
    try:
        # Magento 1
        cr.execute("DESCRIBE core_resource")
        cr.execute("SELECT code, version, data_version FROM core_resource")
        module_res = cr.fetchall()
    except MySQLProgrammingError:
        module_res = None
    if not module_res:
        try:
            # Magento 2
            cr.execute("DESCRIBE setup_module")
            cr.execute("SELECT module, schema_version, data_version FROM setup_module")
            module_res = cr.fetchall()
        except MySQLProgrammingError:
            module_res = None

    modules = {}
    if module_res:
        for row in module_res:
            modules[row[0]] = row[1:]
    # example from Odoo
    # cr.execute("SELECT name, latest_version FROM ir_module_module WHERE state = 'installed'")
    # modules = dict(cr.fetchall())
    # try:
    #     import odoo.release
    #     version = odoo.release.version
    #     version_info = odoo.release.version_info
    #     major_version = odoo.release.major_version
    # except ImportError as e:
    #     print('Cannot import odoo.release (using defaults)' + str(e))
    #     version = '0.0'
    #     version_info = '0.0'
    #     major_version = '0.0'

    manifest = {
        'db_name': db_name,
        # 'version': version,
        # 'version_info': version_info,
        # 'major_version': major_version,
        'mysql_version': mysql_version,
        'modules': modules,
    }
    return manifest
