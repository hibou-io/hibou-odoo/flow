from flow.tools.args import parser
from flow.tools.run import run

parser.add_argument('dest_dir', type=str,
                    help='destination directory')

args = parser.parse_args()
dest_dir = args.dest_dir

print('Installing to  %s' % (dest_dir, ))
run(['cp', '-R', '/flow', dest_dir])
