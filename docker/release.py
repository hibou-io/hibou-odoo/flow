import datetime

from flow.tools.args import parser
from flow.tools.run import run, CalledProcessError


parser.add_argument('from_image', type=str,
                    help='from Image (e.g. registry.gitlab.com/yourteam/yourproject:1.0-2020-06-20 )')
parser.add_argument('to_image', type=str,
                    help='to Image (e.g. registry.gitlab.com/yourteam/yourproject:1.0 )')

# Optionals with defaults
parser.add_argument('--pull-fail-strat', dest='pull_fail_strat', default=None,
                    help='when failure on pull, \'backoff-days\' try to parse date from end of string and back off 1 day at a time')
parser.add_argument('--pull-fail-strat-limit', dest='pull_fail_strat_limit', default=2,
                    help='how many "attempts" when pull failure (increase when using a pull-fail-strat)')
parser.add_argument('--fail-push-limit', dest='fail_push_limit', default=2,
                    help='how many "attempts" when push fails')

args = parser.parse_args()
# Required
from_image, to_image = args.from_image, args.to_image
# Optional or has defaults
pull_fail_strat, pull_fail_strat_limit, fail_push_limit = args.pull_fail_strat, int(args.pull_fail_strat_limit), int(args.fail_push_limit)

print('Pulling %s' % (from_image, ))

# TODO Tests?
# run(['docker', 'pull', 'xxx'])
# RAISED
# subprocess.CalledProcessError: Command '['docker', 'pull', 'from_imagex']' returned non-zero exit status 1.
# run(['docker', 'pull', from_image])

date_format = 'YYYY-mm-dd'

pull_count = 0
pull_success = False
pull_fail_strat_limit = max(pull_fail_strat_limit, 1)

while pull_count < pull_fail_strat_limit and not pull_success:
    try:
        pull_count += 1
        print('Pulling %s (try %s)' % (from_image, pull_count))
        run(['docker', 'pull', from_image])
        pull_success = True
    except CalledProcessError:
        if pull_fail_strat:
            if pull_fail_strat in ('backoff-day', 'backoff-days'):
                # parse date
                from_image_date = from_image[-len(date_format):]
                from_image_date = datetime.date(*(int(d.lstrip('0')) for d in from_image_date.split('-')))
                from_image_date = from_image_date - datetime.timedelta(days=1)
                from_image = from_image[:-len(date_format)] + str(from_image_date)
                # we have a new image to try
            else:
                raise Exception('Unknown --pull-fail-strat "%s"' % (pull_fail_strat, ))

if pull_success:
    print('Pull Success! (%s tries)' % (pull_count, ))
    push_count = 0
    push_success = False
    fail_push_limit = max(fail_push_limit, 1)
    print('Tagging %s TO %s' % (from_image, to_image))
    run(['docker', 'tag', from_image, to_image])

    while push_count < fail_push_limit and not push_success:
        try:
            push_count += 1
            print('Pushing %s (try %s)' % (to_image, push_count))
            run(['docker', 'push', to_image])
            push_success = True
        except CalledProcessError:
            pass

    if push_success:
        print('Push Success! (%s tries)' % (push_count, ))
        exit(0)
    else:
        exit(1)
exit(2)
