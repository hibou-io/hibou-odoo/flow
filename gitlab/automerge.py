import os
from argparse import ArgumentParser
import json
from pprint import pprint
from time import sleep
from urllib.request import urlopen, Request
from urllib.error import HTTPError

parser = ArgumentParser()
# Intend to use like `gitlab-automerge`  (only optional parameters, all required ones will be derived from environment variables
# Uses default environment variables otherwise
CI_COMMIT_REF_NAME = os.getenv('CI_COMMIT_REF_NAME', '')
CI_PROJECT_URL = os.getenv('CI_PROJECT_URL', '')
CI_PROJECT_ID = os.getenv('CI_PROJECT_ID', '')
GITLAB_TOKEN = os.getenv('GITLAB_TOKEN', '')
GITLAB_USER_ID = os.getenv('GITLAB_USER_ID', '')

# source_branch
# is ($CI_COMMIT_REF_NAME)
parser.add_argument('--source-branch', dest='source_branch', type=str, default=CI_COMMIT_REF_NAME,
                    help='Source Branch')

# target_branch
# from (echo $CI_COMMIT_REF_NAME | sed "s{.*\/\(.*\)\/.*{\1{g") or --destination-branch "${RELEASE}-test"
target_branch = ''
if CI_COMMIT_REF_NAME.find('/') >= 0:
    target_branch = CI_COMMIT_REF_NAME.split('/')[1]
parser.add_argument('--target-branch', dest='target_branch', type=str, default=target_branch,
                    help='Target Branch')

# gitlab_url
# from (echo $CI_PROJECT_URL | sed 's{\(^https://[^/]*/\).*{\1{g') or --gitlab-url "https://mygitlab.com/"
gitlab_url = ''
project_url = CI_PROJECT_URL
project_url_pieces = project_url.split('/')
if len(project_url_pieces) >= 3:
    gitlab_url = '/'.join(project_url_pieces[:3])
parser.add_argument('--gitlab-url', dest='gitlab_url', type=str, default=gitlab_url,
                    help='GitLab URL (e.g. https://mygitlab.com)')

# gitlab_project_id
# is ($CI_COMMIT_REF_NAME)
parser.add_argument('--gitlab-project-id', dest='gitlab_project_id', type=str, default=CI_PROJECT_ID,
                    help='GitLab Project ID (e.g. 1002)')

# gitlab_token
# is (GITLAB_TOKEN)
parser.add_argument('--gitlab-token', dest='gitlab_token', type=str, default=GITLAB_TOKEN, help='GitLab Token')

# with defaults
parser.add_argument('--gitlab-assign-user-id', dest='gitlab_assign_user_id', type=str, default=GITLAB_USER_ID,
                    help='Merge Request Assign User')
parser.add_argument('--remove-source', dest='remove_source', default=False, const=True, nargs='?',
                    help='should remove source branch when merged')
parser.add_argument('--automerge', dest='automerge', default=False, const=True, nargs='?',
                    help='should automatically merge')

# Future?
# parser.add_argument('-o', '--odoo', dest='odoo_url', default='', type=str, help='URL of Odoo to Notify')
# parser.add_argument('-z', '--odoo-key', dest='odoo_key', default='', type=str, help='Odoo Shared Secret Key')
# need something like odoo repo or project

args = parser.parse_args()

# Special Requirements due to potentially coming from environment
exception_message = 'Missing Required Parameter "%s" (should be possible from CI Environment)\n' + \
                    'Maybe consider the following to get all of the needed combinations:\n' + r'''
$ ENVS=`env | grep "CI_\|GITLAB_" | sed -n '/^[^\t]/s/=.*//p' | sed '/^$/d' | sed 's/^/-e /g' | tr '\n' ' '`
$ docker run $ENVS flow gitlab-automerge
'''

source_branch = args.source_branch
if not source_branch:
    raise Exception(exception_message % 'source_branch')
target_branch = args.target_branch
if not target_branch:
    raise Exception(exception_message % 'target_branch')
gitlab_url = args.gitlab_url
if not gitlab_url:
    raise Exception(exception_message % 'gitlab_url')
gitlab_project_id = args.gitlab_project_id
if not gitlab_project_id:
    raise Exception(gitlab_project_id % 'source_branch')

# Most likely to be missing.
gitlab_token = args.gitlab_token
if not gitlab_token:
    raise Exception('Missing --gitlab-token or Environment PRIVATE_TOKEN\n'
                    'This should be a personal access token for a user capable of making merge requests.\n'
                    'The Settings->CI/CD->Variables section can be used to pass it into the runner.')

remove_source, automerge, gitlab_assign_user_id  = args.remove_source, args.automerge, args.gitlab_assign_user_id

# Requirements Satisfied.
url = gitlab_url + '/api/v4/projects/'

def gitlab_try_merge(merge_request, attempts=0):
    print('Trying to merge:')
    pprint(merge_request)
    if merge_request['merge_status'] in ('can_be_merged', 'checking'):
        iid = merge_request['iid']
        body = {}
        if merge_request['merge_status'] == 'checking':
            body['merge_when_pipeline_succeeds'] = True
        request = Request(url + gitlab_project_id + '/merge_requests/' + str(iid) + '/merge',
                          method='PUT',
                          data=json.dumps(body).encode())
        request.add_header('PRIVATE-TOKEN', gitlab_token)
        try:
            response = urlopen(request)
            data = response.read()
            encoding = response.info().get_content_charset('utf-8')
            
            merge = json.loads(data.decode(encoding))
            print('Merge response:')
            pprint(merge)
        except HTTPError as e:
            if attempts < 5:
                print('Sleeping for HTTPError: ' + str(e))
                sleep(5)
                gitlab_try_merge(merge_request, attempts+1)
            else:
                print('Merge request failed.')
    else:
        print('Tried to merge, but cannot for status ' + str(merge_request['merge_status']))


# Get open requirements
request = Request(url + gitlab_project_id + '/merge_requests?state=opened')
request.add_header('PRIVATE-TOKEN', gitlab_token)
response = urlopen(request)
data = response.read()
encoding = response.info().get_content_charset('utf-8')

open_merge_requests = json.loads(data.decode(encoding))
pprint(open_merge_requests)
for merge_req in open_merge_requests:
    if merge_req.get('source_branch') == source_branch and merge_req.get('target_branch') == target_branch:
        # This one is a duplicate, can try to automerge
        print('Existing merge request found.')
        if automerge:
            if merge_req['merge_status'] in ('can_be_merged', 'checking') and not merge_req['work_in_progress']:
                gitlab_try_merge(merge_req)
            else:
                pass
        break
else:
    # Didn't find existing request
    body = {
        'id': gitlab_project_id,
        'source_branch': source_branch,
        'target_branch': target_branch,
        'remove_source_branch': remove_source,
        'title': '%s%s into %s' % ('' if automerge else 'WIP: ', source_branch, target_branch),
    }

    if gitlab_assign_user_id:
        body['assignee_id'] = gitlab_assign_user_id

    if automerge:
        # Note that this does not seem to work on creation and is handled in gitlab_try_merge
        body['merge_when_pipeline_succeeds'] = True

    request = Request(url + gitlab_project_id + '/merge_requests')
    request.add_header('PRIVATE-TOKEN', gitlab_token)
    request.add_header('Content-Type', 'application/json')
    response = urlopen(request, json.dumps(body).encode())
    data = response.read()
    encoding = response.info().get_content_charset('utf-8')

    merge_request = json.loads(data.decode(encoding))
    pprint(merge_request)
    if automerge:
        gitlab_try_merge(merge_request)
