FROM python:3.9-slim

RUN set -x; \
    mkdir /flow \
    && apt update \
    && apt upgrade -y \
    && apt install -y --no-install-recommends \
        git \
        openssh-client \
        rsync \
        default-libmysqlclient-dev \
        default-mysql-client \
        gcc \
    && rm -rf /root/.cache \
    && apt clean \
    && rm -rf /var/lib/apt/lists/* \
    ;
# TODO separate mysql out like postgres
COPY . /flow
RUN set -x; \
    pip3 install -r /flow/requirements.txt \
    ;

WORKDIR /flow
ENTRYPOINT ["/flow/entrypoint.sh"]
CMD ["bash"]
