#!/bin/sh
set -e

export PYTHONUNBUFFERED=1
export PATH="${PWD}/../:${PATH}"
# TODO ideally the odoo package would come from somewhere else
export PYTHONPATH="${PWD}/../:/opt/odoo/odoo:${PYTHONPATH}"

case "$1" in
    migrate-module)
      shift
      exec python3 ./odoo/migrate_module.py "$@"
      ;;
    gitlab-automerge)
      shift
      exec python3 ./gitlab/automerge.py "$@"
      ;;
    odoo-migrate-module)
      shift
      exec python3 ./odoo/migrate_module.py "$@"
      ;;
    odoo-publish)
      shift
      exec python3 ./odoo/publish.py "$@"
      ;;
    odoo-copy)
      shift
      exec python3 ./odoo/copy_db.py "$@"
      ;;
    odoo-delete)
      shift
      exec python3 ./odoo/delete.py "$@"
      ;;
    odoo-backup)
      shift
      exec python3 ./odoo/backup.py "$@"
      ;;
    odoo-restore)
      shift
      exec python3 ./odoo/restore.py "$@"
      ;;
    odoo-upgrade)
      shift
      exec python3 ./odoo/upgrade.py "$@"
      ;;
    odoo-addons-not-installable)
      shift
      exec python3 ./odoo/addons_not_installable.py "$@"
      ;;
    odoo-sql)
      shift
      exec python3 ./odoo/sql.py "$@"
      ;;
    odoo-locust)
      shift
      exec python3 ./odoo/HibouOdooLocust/entrypoint.py "$@"
      ;;
    odoo-active)
      shift
      exec python3 ./odoo/active.py "$@"
      ;;
    magento-backup)
      shift
      exec python3 ./magento/backup.py "$@"
      ;;
    docker-release)
      shift
      exec python3 ./docker/release.py "$@"
      ;;
    self-install)
      shift
      exec python3 ./self/install.py "$@"
      ;;
    *)
        exec "$@"
esac
